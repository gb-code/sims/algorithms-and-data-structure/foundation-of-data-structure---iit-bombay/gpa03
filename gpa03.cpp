#include "header.h"
/* DON"T REMOVE OR CHANGE THE ABOVE HEADER
class twoStacks
{
	int *arr;// array has only non negative elements.
	int size;
	int top1;
    //top1 is index of the topmost element of stack1 and is -1 if stack1 is empty  
    int top2;
    // top2 is index of the topmost element of stack2 and is equal to size if stack2 is empty 
public:
	twoStacks(int n) // constructor
	{
		size = n;
		arr = new int[n];
		top1 = -1; // note this initialization
		top2 = size;// note this initialization
	}
	bool push1(int i);//push i onto stack 1
	bool push2(int i);
	int pop1();// pop top element from stack 1
	int pop2();
    void double_resize();// resize the arr to double its current size
};
*/

/*
Function double_size() must double the size of the array by reallocating the array using the C++ operator new, and copy both the stacks into this newly created array.
It must also deallocate the dynamic memory previously allocated to the arr by using the keyword delete in C++
*/
void twoStacks::double_resize() {
	// WRITE YOUR CODE HERE
    int *arr_new;
    int size_new = 2*size;
    arr_new = new int[size_new];
    int i=0;
    
    for(i=0;i<=top1;i++)
        arr_new[i]=arr[i];
        
    i=0;
    for(i=0;i<size-top2;i++)
        arr_new[size_new-i-1] = arr[size-i-1];
        
    delete [] arr;
    arr = arr_new;
    top2 = size_new - size + top2;
    size = size_new;

}


/*
	If stack1 is not full then the function push1(int i) should add element i into stack1 and return true. 
	Else it should return false
*/
bool twoStacks::push1(int i) {
	//WRITE YOUR CODE HERE
    if(top1 < top2-1)
    {
        arr[++top1] = i;
        return 1;
    }
    else
    {
        return 0;
    }


	// RETURN A BOOLEAN VALUE HERE
}

/*
	If stack2 is not full then the function push2(int i) should add element i into stack2 and return true. 
	Else it should return false
*/

bool twoStacks::push2(int i) {
	//WRITE YOUR CODE HERE
    if(top1 < top2 -1)
    {
        arr[--top2] = i;
        return true;
    }
    else 
    {
        return false;
    }


	// RETURN A BOOLEAN VALUE HERE
}

/*
If stack1 is not empty then the function pop1() should pop the top most element from the stack and return its value.
If stack1 is empty then the function pop1() must return -1
You may assume that the arr contains only non-negative integers
*/
int twoStacks::pop1() {
	// FUNCTION MUST RETURN AN INTEGER VALUE
	//WRITE YOUR CODE HERE
	if(top1 >= 0){
	    return arr[top1--];
	    
	}
	else{
	    return -1;
	}

}



/*
If stack2 is not empty then the function pop2() should pop the top most element from the stack and return its value.
If stack2 is empty then the function pop2() must return -1
You may assume that the arr contains only non-negative integers
*/
int twoStacks::pop2() {
	// FUNCTION MUST RETURN AN INTEGER VALUE
	//WRITE YOUR CODE HERE
	if(top2 < size)
	{
	    return arr[top2++];
	    
	}
	else{
	    return -1;
	}


}